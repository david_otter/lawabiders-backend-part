# lawabiders application

## Prerequisites

Both for the back end and front end application check

* nodejs [official website](https://nodejs.org/en/) - nodejs includes [npm](https://www.npmjs.com/) (node package manager)

Just for the backend application:

* mongodb [official installation guide](https://docs.mongodb.org/manual/administration/install-community/)


## Setup (before first run)

go to your project root folder via command line
```
cd path/to/workspace/lawabiders-backend
```

**install backend dependencies**

```
npm install
```

**set up your database**

* create a new directory where your database will be stored (it's a good idea to separate data and business logic - the data directory should be on a different place than your app)
* start the database server 
```
mongod --dbpath relative/path/to/database
```
** install parser dependencies and execute it once to fill the database

* add parsed BGB to database
* add standard user (username: admin, password: admin)
```
cd parser/
npm install
node index.js
```

**set up environment configuration**

A valid config.js is already in place in the director `config/config.js` as it does not contain any sensible informations (such as credentials).
Of course, it can be changed in order to meet special needs. 

## running

start the web server from the root directory of the project

```
node server.js
```