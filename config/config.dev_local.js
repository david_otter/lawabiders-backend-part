var Config = {};
Config.db = {};
Config.app = {};
Config.auth = {};

Config.db.host = 'localhost:27017';
Config.db.name = 'lawdb';

// Use environment defined port or 3000
Config.app.port = process.env.PORT || 3000;

Config.auth.jwtSecret = "RQ2LZ*sPwf7jo%mv9mxW63Bdbwm^%9B1";

module.exports = Config;