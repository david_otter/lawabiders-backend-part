var LawCode = require('./../schema/codeSchema');

exports.postLawCode = function (req, res) {
  var lawCode = new LawCode(req.body);
  lawCode.save(function (err, m) {
    if (err) {
      return res.status(500).send(err);
    }
    res.status(201).json(m);
  });
};

exports.updateLawCode = function (req, res) {
  LawCode.findOneAndUpdate({name: req.params.lawCode}, req.body,
      {new: true}, function (err, m) {
        if (err) {
          return res.status(500).send(err);
        }
        res.status(201).json(m);
      });
};

exports.getLawCodes = function (req, res) {
  LawCode.find(function (err, m) {
    if (err) {
      return res.status(500).send(err);
    }
    res.status(201).json(m);
  });
};

exports.getLawCode = function (req, res) {
  LawCode.findOne({name: req.params.lawCode}, function (err, m) {
    if (err) {
      return res.status(500).send(err);
    }
    res.status(201).json(m);
  });
};