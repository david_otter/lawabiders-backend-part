var ParagraphSchema = require('./../schema/paragraphSchema');
var ParagraphHelperController = require('./paragraphHelperController');
var UserController = require('./../../user/userController');
var mongoose = require('mongoose');

exports.postComment = function (req, res) {
  var comment = ParagraphSchema.Comments(req.body);
  // only the authorized user can be the paragraph owner
  if (!req.user.equals(comment.owner)) {
    return res.sendStatus(401);
  }
  if (comment.position == null) {
    return res.status(400).send("Position is missing");
  }
  if (!comment.text) {
    return res.status(400).send("Text is missing");
  }
  comment._id = mongoose.Types.ObjectId();
  comment.username = req.user.firstName + " " + req.user.lastName;
  ParagraphHelperController.findController(req.params.paragraph_id, res, function (Controller) {
    Controller.findByIdAndUpdate(req.params.paragraph_id, {"$push": {comments: comment}},
        {new: true}, function (err, m) {
          if (err) {
            return res.status(500).send(err);
          }
          return UserController.pushCommentId(comment.owner, comment._id, res, m);
        });
  });
};

// Create endpoint /api/paragraphs/:paragraph_id/comments for GET
exports.getComments = function (req, res) {
  ParagraphHelperController.getParagraph(req, res, function (paragraph) {
    res.json(paragraph.comments);
  });
};

// Can't update owner!
exports.updateComment = function (req, res) {
  // only the authorized user can be the paragraph owner
  if (!req.user.equals(req.body.owner)) {
    return res.sendStatus(401);
  }
  if (req.body.position == null) {
    return res.status(400).send("Position is missing");
  }
  if (!req.body.text) {
    return res.status(400).send("Text is missing");
  }
  var username = req.user.firstName + " " + req.user.lastName;
  ParagraphHelperController.findControllerGeneralized({'comments._id': req.params.comment_id}, res, function (Controller) {
    Controller.findOneAndUpdate({'comments._id': req.params.comment_id}, {
      '$set': {
        'comments.$.text': req.body.text,
        'comments.$.position': req.body.position, 'comments.$.username': username
      }
    }, {new: true}, function (err, m) {
      if (err) {
        return res.status(500).send(err);
      }
      return res.json(m);
    });
  });
};

exports.deleteComment = function (req, res) {
  ParagraphHelperController.findControllerGeneralized({'comments._id': req.params.comment_id}, res, function (Controller) {
    Controller.findOne({'comments._id': req.params.comment_id}, function (err, m) {
      var comment = m.comments.id(req.params.comment_id);
      var paragraph = Controller(m);
      if (err) {
        return res.status(500).send(err);
      }
      if (!req.user.equals(comment.owner)) {
        return res.sendStatus(401);
      }
      paragraph.comments.pull({_id: comment._id});
      paragraph.save(function (err, para) {
        if (err) {
          return res.status(500).send(err);
        }
        return UserController.deleteCommentId(comment.owner, comment._id, res, para);
      });
    });
  });
};

exports.getComment = function (req, res) {
  ParagraphHelperController.findControllerGeneralized({'comments._id': req.params.comment_id}, res, function (Controller) {
    Controller.findOne({'comments._id': req.params.comment_id}, function (err, m) {
      if (err) {
        return res.status(500).send(err);
      }
      if (m) {
        var comment = m.comments.id(req.params.comment_id);
        if (comment) {
          return res.status(200).json(comment);
        }
      }
      return res.status(400).json("Comment id not found");
    });
  });
};