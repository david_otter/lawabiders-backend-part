var ParagraphSchema = require('./../schema/paragraphSchema');
var ParagraphHelperController = require('./paragraphHelperController');
var UserController = require('./../../user/userController');
var mongoose = require('mongoose');

/*
 * Should only be used for testing - development scope
 * Needs authentication for production stage
 * No full check for (intentionally) malformed request body
 */
exports.postStaticLink = function (req, res) {
  var staticLink = ParagraphSchema.StaticLinks(req.body);
  if (staticLink.begin >= staticLink.end) {
    return res.status(400).send("Begin has to be less than end");
  }

  ParagraphHelperController.findController(req.params.paragraph_id, res, function (Controller) {
    Controller.findByIdAndUpdate(req.params.paragraph_id, {"$push": {staticLinks: staticLink}},
        {new: true}, function (err, m) {
          if (err) {
            return res.status(500).send(err);
          }
          return res.status(200).json(m);
        });
  });
};

// Create endpoint /api/paragraphs/:paragraph_id/staticLinks for GET
exports.getStaticLinks = function (req, res) {
  ParagraphHelperController.getParagraph(req, res, function (paragraph) {
    res.json(paragraph.staticLinks);
  });
};

exports.postUserLink = function (req, res) {
  var userLink = ParagraphSchema.UserLinks(req.body);
  // only the authorized user can be the paragraph owner
  if (!req.user.equals(userLink.owner)) {
    return res.sendStatus(401);
  }
  if (!userLink.paragraph) {
    return res.status(400).send("Link reference is missing")
  }
  userLink._id = mongoose.Types.ObjectId();
  ParagraphHelperController.getParagraph(req, res, function (targetParagraph) {
    ParagraphHelperController.findController(req.params.paragraph_id, res, function (Controller) {
      userLink.title = targetParagraph.lawCode + " " + targetParagraph.title;
      userLink.username = req.user.firstName + " " + req.user.lastName;
      Controller.findByIdAndUpdate(req.params.paragraph_id, {"$push": {userLinks: userLink}},
          {new: true}, function (err, m) {
            if (err) {
              return res.status(500).send(err);
            }
            return UserController.pushUserLinkId(userLink.owner, userLink._id, res, m);
          });
    });
    // Ugly but it works
  }, userLink.paragraph);
};

// Create endpoint /api/paragraphs/:paragraph_id/staticLinks for GET
exports.getUserLinks = function (req, res) {
  ParagraphHelperController.getParagraph(req, res, function (paragraph) {
    res.json(paragraph.userLinks);
  });
};

exports.deleteUserLink = function (req, res) {
  ParagraphHelperController.findControllerGeneralized({'userLinks._id': req.params.userLink_id}, res, function (Controller) {
    Controller.findOne({'userLinks._id': req.params.userLink_id}, function (err, m) {
      var userLink = m.userLinks.id(req.params.userLink_id);
      var paragraph = Controller(m);
      if (err) {
        return res.status(500).send(err);
      }
      if (!req.user.equals(userLink.owner)) {
        return res.sendStatus(401);
      }
      paragraph.userLinks.pull({_id: userLink._id});
      paragraph.save(function (err, para) {
        if (err) {
          return res.status(500).send(err);
        }
        return UserController.deleteUserLinkId(userLink.owner, userLink._id, res, para);
      });
    });
  });
};

exports.getUserLink = function (req, res) {
  ParagraphHelperController.findControllerGeneralized({'userLinks._id': req.params.userLink_id}, res, function (Controller) {
    Controller.findOne({'userLinks._id': req.params.userLink_id}, function (err, m) {
      if (err) {
        return res.status(500).send(err);
      }
      if (m) {
        var userLink = m.userLinks.id(req.params.userLink_id);
        if (userLink) {
          return res.status(200).json(userLink);
        }
      }
      return res.status(400).json("UserLink id not found");
    });
  });
};