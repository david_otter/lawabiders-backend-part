var ParagraphSchema = require('./../schema/paragraphSchema');
var ParagraphHelperController = require('./paragraphHelperController');

/*
 * Should only be used for testing - development scope
 * Needs authentication for production stage
 * No full check for (intentionally) malformed request body
 */
exports.postParagraph = function (req, res) {
  var body = '';

  var paragraph = new ParagraphSchema.Paragraph(req.body);

  // Search for equal paragraphs that is not older than the request
  ParagraphSchema.Paragraph.findOne({
        $and: [{'lawCode': req.body.lawCode},
          {'number': req.body.number}, {'version': {'$gte': req.body.version}}]
      },
      function (err, found) {
        if (err) {
          return res.status(500).send(err);
        }
        // if newer object already exists exit
        if (found) {
          if (found.text.valueOf() === req.body.text.valueOf()) {
            return res.status(200).send('Comparable resource already exists');
          } else {
            body += "Newer Object already exists\n\n";
            // save the document in the archive collection
            return archive();
          }
        }
        // find an older paragraph
        ParagraphSchema.Paragraph.findOne({
              $and: [{'lawCode': req.body.lawCode},
                {'number': req.body.number}, {'version': {'$lt': req.body.version}}]
            },
            function (err, found) {
              if (err) {
                return res.status(500).send(err);
              }

              // An older paragraph with equal text field is found and its version is updated
              if (found && found.text.valueOf() === req.body.text.valueOf()) {
                found.version = req.body.version;
                ParagraphSchema.Paragraph.update(found, function (err, m) {
                  if (err) {
                    return res.status(500).send(err);
                  }
                  return res.status(200).send("Existing paragraph version successfully updated\n"
                      + JSON.stringify(m, null, 2));
                });
                return;
              }

              // otherwise save the new object
              paragraph.save(function (err, m) {
                if (err) {
                  return res.status(500).send(err);
                }
                // and clean up - move the old paragraph to archive
                if (found) {
                  var oldParagraph = new ParagraphSchema.ParagraphArchive(found.toObject());
                  oldParagraph.save(function (err) {
                    if (err) {
                      return res.status(500).send(err);
                    }
                    // wait for success before deleting
                    ParagraphSchema.Paragraph.findByIdAndRemove(found.id, function (err) {
                      if (err) {
                        return res.status(500).send(err);
                      }
                      return res.status(201).send("Old paragraph moved to archive\n"
                          + "New paragraph successfully added\n"
                          + JSON.stringify(m, null, 2));
                    });
                  });
                  return;
                }
                return res.status(201).send("New paragraph successfully added\n"
                    + JSON.stringify(m, null, 2));
              });
            });
      });

  function archive() {
    var paragraphArchive = new ParagraphSchema.ParagraphArchive(req.body);
    // The archive has to make sure that it only contains different objects (regarding timestamp and text)
    ParagraphSchema.ParagraphArchive.find({
          $and: [{'lawCode': req.body.lawCode},
            {'number': req.body.number},
            {'version': {'$eq': req.body.version}}]
        },
        function (err, paragraphs) {
          if (err) {
            return res.status(500).send(body + err);
          }
          if (paragraphs.length > 0) {
            return res.status(200).send(body + 'Comparable resource already exists in archive');
          }
          ParagraphSchema.ParagraphArchive.find({
                $and: [{'lawCode': req.body.lawCode},
                  {'number': req.body.number}]
              },
              function (err, paragraphs) {
                if (err) {
                  return res.status(500).send(body + err);
                }

                // function to save the paragraph
                function save() {
                  paragraphArchive.save(function (err, m) {
                    if (err) {
                      return res.status(500).send(body + err);
                    }
                    return res.status(201).send(body + "New paragraph successfully added to archive\n"
                        + JSON.stringify(m, null, 2));
                  })
                }

                if (paragraphs.length > 0) {
                  for (var i = 0; i < paragraphs.length; i++) {
                    // search for existing older paragraphs with the same text
                    if (paragraphs[i].version < paragraphArchive.version &&
                        paragraphs[i].text.valueOf() === req.body.text.valueOf()) {
                      return res.status(200).send(body + 'Comparable resource already exists in archive');
                      // search for existing newer paragraphs with the same text and update their version
                    } else if (paragraphs[i].version > paragraphArchive.version &&
                        paragraphs[i].text.valueOf() === paragraphArchive.text.valueOf()) {
                      return ParagraphSchema.ParagraphArchive.update({_id: paragraphs[i]._id},
                          {$set: {version: paragraphArchive.version}}, function (err, m) {
                            if (err) {
                              return res.status(500).send(body + err);
                            }
                            return res.status(200).send(body + "Existing paragraph version successfully updated\n"
                                + JSON.stringify(m, null, 2));
                          });
                    }
                  }
                }
                return save();
              });
        });
  }
};

// Create endpoint /api/paragraphs for GET
exports.getParagraphs = function (req, res) {
  ParagraphSchema.Paragraph.find(function (err, paragraphs) {
    if (err) {
      res.status(500).send(err);
      return;
    }
    res.json(paragraphs);
  });
};

// Create endpoint /api/paragraphs/min for GET
exports.getParagraphsMin = function (req, res) {
  ParagraphSchema.Paragraph.find({}, {
    'lawCode': 1,
    'title': 1,
    'userLinks': 1,
    'staticLinks': 1,
    'number': 1
  }, function (err, paragraphs) {
    if (err) {
      res.status(500).send(err);
      return;
    }
    res.json(paragraphs);
  });
};

// Create endpoint /api/archiveParagraphs for GET
exports.getArchiveParagraphs = function (req, res) {
  ParagraphSchema.ParagraphArchive.find(function (err, paragraphs) {
    if (err) {
      res.status(500).send(err);
      return;
    }
    res.json(paragraphs);
  });
};

// Create endpoint /api/paragraphs/:paragraph_id for GET
exports.getParagraph = function (req, res) {
  ParagraphHelperController.getParagraph(req, res, function (paragraph) {
    res.json(paragraph);
  });
};

exports.getParagraphNumber = function (req, res) {
  ParagraphSchema.Paragraph.findOne({
        $and: [{'lawCode': req.params.lawCode}, {'number': req.params.number}]
      },
      function (err, paragraph) {
        if (err) {
          return err;
        }
        res.json(paragraph);
      });
};

exports.getPaginatedParagraphs = function (req, res) {
  var params = req.params.paragraph_id.split("&");
  var inDescendingOrder = params[1] && (params[1].split("=")[1] == "true");
  var query;

  if (inDescendingOrder) {
    query = {"$lte": params[0]};
  } else {
    query = {"$gte": params[0]};
  }

  ParagraphSchema.Paragraph.find({$and: [{"lawCode": req.params.lawCode}, {"_id": query}]}, {
    'lawCode': 1, 'text': 1,
    'title': 1, 'userLinks': 1, 'staticLinks': 1, 'number': 1, 'comments': 1
  }).sort({'_id': inDescendingOrder ? -1 : 1}).limit(50).exec(
      function (err, paragraphs) {
        if (err) {
          return res.status(500).send(err);
        }
        if (inDescendingOrder) {
          res.json(paragraphs.reverse());
        } else {
          res.json(paragraphs);
        }
      });
};

exports.getArchiveParagraphNumber = function (req, res) {
  ParagraphSchema.ParagraphArchive.find({
        $and: [{'lawCode': req.params.lawCode}, {'number': req.params.number}]
      },
      function (err, paragraph) {
        if (err) {
          return err;
        }
        res.json(paragraph);
      });
};
