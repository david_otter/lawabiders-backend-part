var ParagraphSchema = require('./../schema/paragraphSchema');

exports.getParagraph = function (req, res, callback, id) {
  // no clean code - still like it :)
  ParagraphSchema.Paragraph.findById(id ? id : req.params.paragraph_id, function (err, paragraph) {
    if (err) {
      return res.status(500).send(err);
    }
    if (!paragraph) {
      // search in the archive if the paragraph_id was not found
      return ParagraphSchema.ParagraphArchive.findById(req.params.paragraph_id, function (err, paragraph) {
        if (err) {
          return res.status(500).send(err);
        }
        if (!paragraph) {
          return res.status(400).send("The paragraph_id was not found");
        }
        return callback(paragraph);
      });
    }
    return callback(paragraph);
  });
};

function find(term, res, callback) {
  var count = 2;

  function notFound() {
    count--;
    if (count == 0) {
      return res.status(400).send("Id not found");
    }
  }

  ParagraphSchema.Paragraph.findOne(term, function (err, paragraph) {
    if (err) {
      return res.status(500).send(err);
    }
    if (paragraph) {
      return callback(ParagraphSchema.Paragraph);
    }
    notFound();
  });
  ParagraphSchema.ParagraphArchive.findOne(term, function (err, paragraph) {
    if (err) {
      return res.status(500).send(err);
    }
    if (paragraph) {
      return callback(ParagraphSchema.ParagraphArchive);
    }
    notFound();
  });
}

exports.findController = function (id, res, callback) {
  find({_id: id}, res, callback);
};

exports.findControllerGeneralized = function (term, res, callback) {
  find(term, res, callback);
};