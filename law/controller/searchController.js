var ParagraphSchema = require('./../schema/paragraphSchema');
var _ = require('underscore');

exports.postSearch = function (req, res) {
  if (req.body.after && req.body.after >= Date.now()) {
    return res.status(401).send('Date is in the future');
  }
  if (req.body.after && req.body.before && req.body.after >= req.body.before) {
    return res.status(401).send('Before date has to be newer than after to get a time range');
  }

  // add the search term
  var query = {};
  if (req.body.searchTerm) {
    query = {$text: {$search: req.body.searchTerm}};
  }

  // add the date filters
  if (req.body.after || req.body.before) {
    query.version = {};
    if (req.body.after) {
      query.version.$gt = req.body.after;
    }
    if (req.body.before) {
      query.version.$lt = req.body.before;
    }
  }

  // add law code filter
  if (req.body.lawCode) {
    query.lawCode = req.body.lawCode;
  }

  // Synchronization helper
  var counter = 1;
  var result = [];

  function searchDone(paragraphs) {
    counter--;
    result.push.apply(result, paragraphs);
    if (counter == 0) {
      return res.status(200).json(result);
    }
  }

  // search archive
  if (req.body.searchArchive == true) {
    counter++;
    ParagraphSchema.ParagraphArchive.find(query, {score: {$meta: "textScore"}})
        .sort({score: {$meta: 'textScore'}}).limit(75).exec(function (err, result) {
      if (err) {
        return res.status(500).send(err);
      }
      searchDone(result);
    });
  }

  // remove the version attribute for search current law
  query = _.omit(query, 'version');

  // search current law
  if (req.body.searchCurrentLaw || !req.body.hasOwnProperty('searchCurrentLaw')) {
    counter++;
    ParagraphSchema.Paragraph.find(query, {score: {$meta: "textScore"}})
        .sort({score: {$meta: 'textScore'}}).limit(75).exec(function (err, result) {
      if (err) {
        return res.status(500).send(err);
      }
      searchDone(result);
    });
  }

  searchDone([]);
};