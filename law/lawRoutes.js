module.exports = lawRoutes;

function lawRoutes(passport) {

  var paragraphController = require('./controller/paragraphController');
  var linkController = require('./controller/linkController');
  var commentController = require('./controller/commentController');
  var searchController = require('./controller/searchController');
  var codeController = require('./controller/codeController');

  var router = require('express').Router();
  var unless = require('express-unless');

  var mw = passport.authenticate('jwt', {session: false});
  mw.unless = unless;

  //middleware - POST allowed for testing !!
  router.use(mw.unless({method: ['GET', 'OPTIONS']}));

  router.route('/paragraphs')
      .post(paragraphController.postParagraph)
      .get(paragraphController.getParagraphs);

  router.route('/paragraphs/min')
      .get(paragraphController.getParagraphsMin);

  router.route('/archive/paragraphs')
      .get(paragraphController.getArchiveParagraphs);

  /**
   * if the current law gives no result the operation will be executed on the archive
   */
  router.route('/paragraphs/:paragraph_id')
      .get(paragraphController.getParagraph);

  /**
   * get the paragraph identified by the combination of lawCode and number (current law)
   */
  router.route('/lawCodes/:lawCode/:number')
      .get(paragraphController.getParagraphNumber);

  /**
   * get up to 50 paragraphs including the paragraph_id and up
   * &before=true:
   *   optional parameter to get the paragraph_id and up to 50 paragraphs before
   *   default=false
   */
  router.route('/pagination/:lawCode/:paragraph_id')
      .get(paragraphController.getPaginatedParagraphs);

  /**
   * get or post a law Code
   */
  router.route('/lawCodes')
      .get(codeController.getLawCodes)
      .post(codeController.postLawCode);

  /**
   * get or update a lowCode based on its identifier
   */
  router.route('/lawCodes/:lawCode')
      .get(codeController.getLawCode)
      .put(codeController.updateLawCode);

  /**
   * get the paragraphs identified by the combination of lawCode and number (archive)
   */
  router.route('/archive/lawCode/:lawCode/:number')
      .get(paragraphController.getArchiveParagraphNumber);

  /**
   * if the current law gives no result the operation will be executed on the archive
   */
  router.route('/paragraphs/:paragraph_id/static-links')
      .post(linkController.postStaticLink)
      .get(linkController.getStaticLinks);

  /**
   * if the current law gives no result the operation will be executed on the archive
   */
  router.route('/paragraphs/:paragraph_id/user-links')
      .post(linkController.postUserLink)
      .get(linkController.getUserLinks);

  router.route('/user-links/:userLink_id')
      .delete(linkController.deleteUserLink)
      .get(linkController.getUserLink);

  /**
   * if the current law gives no result the operation will be executed on the archive
   */
  router.route('/paragraphs/:paragraph_id/comments')
      .post(commentController.postComment)
      .get(commentController.getComments);

  router.route('/comments/:comment_id')
      .put(commentController.updateComment)
      .delete(commentController.deleteComment)
      .get(commentController.getComment);

  /**
   * implement the search as POST to add search parameters and filters in the body
   *
   * optional: | "searchTerm" : "enter your search query here" |
   * optional: | "searchArchive" : boolean | default: false -> search in the Archive
   * optional: | "searchCurrentLaw" : boolean | default: true -> search currentLaw
   * optional: | "before" : Date | e.g. "2010-09-19"
   * -> only return laws that were established before this date
   * Note: This will not affect current law!
   * optional: | "after" : Date | e.g. "2010-09-19"
   * -> only return laws that were established after this date
   * Note: This will not affect current law!
   * Note: Date must not be in the future
   * optional: | "lawCode" : law-code-acronym  | e.g. "BGB"
   * -> Only return paragraphs of this law code
   *
   */
  router.route('/search')
      .post(searchController.postSearch);

  return router;
}