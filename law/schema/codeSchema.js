var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Chapter = new Schema({
  name: {type: String, required: true},
  paragraphs: [String]
});

Chapter.add({
  chapters: [Chapter]
});

var LawCode = new Schema({
  name: {type: String, unique: true, required: true},
  paragraphs: [String]
});

LawCode.add({
  chapters: [Chapter]
});

module.exports = mongoose.model('LawCode', LawCode);
