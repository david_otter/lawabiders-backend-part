var mongoose = require('mongoose'), Schema = mongoose.Schema, ObjectId = Schema.ObjectId;

var Comments = new Schema({
  position: {type: Number, required: true},
  text: {type: String, required: true},
  username: {type: String, required: true},
  owner: {type: ObjectId, required: true}
});

var UserLinks = new Schema({
  paragraph: {type: ObjectId, required: true},
  title: {type: String, required: true},
  username: {type: String, required: true},
  owner: {type: ObjectId, required: true}
});

var StaticLinks = new Schema({
  begin: {type: Number, required: true},
  end: {type: Number, required: true},
  paragraph: {type: ObjectId, required: true}
});

var Paragraph = new Schema({
  lawCode: {type: String, index: true, required: true},
  chapter: {type: ObjectId, sparse: true, default: null},
  title: {type: String},
  text: {type: String},
  staticLinks: [StaticLinks],
  userLinks: [UserLinks],
  comments: [Comments],
  version: {type: Date, index: true, required: true},
  number: {type: String, index: true, required: true},
  processed: Date,
  repealed: {type: Boolean, default: false}
});

Paragraph.index({
      lawCode: "text",
      text: "text",
      title: "text",
      "comments.text": "text"
    },
    {"weights": {lawCode: 12, title: 8, text: 4}},
    {default_language: "german"}
);


exports.Paragraph = mongoose.model('Paragraph', Paragraph);
exports.StaticLinks = mongoose.model('StaticLinks', StaticLinks);
exports.UserLinks = mongoose.model('UserLinks', UserLinks);
exports.Comments = mongoose.model('Comments', Comments);
exports.ParagraphArchive = mongoose.model('ParagraphArchive', Paragraph);

exports.ParagraphSchema = Paragraph;
