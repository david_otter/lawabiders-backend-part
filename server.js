var Config = require('./config/config');
var app = require('./app');
var winston = require('winston');

/**
 * Start the server
  */

app.listen(Config.app.port);
winston.log('info', 'Server startet on port ' + Config.app.port);
