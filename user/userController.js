var Config = require('../config/config.js');
var User = require('./userSchema');
var jwt = require('jwt-simple');
var paragraphHelperController = require('./../law/controller/paragraphHelperController');

module.exports.login = function(req, res){

    if(!req.body.username && !req.body.email){
        res.status(400).send('username or email required');
        return;
    }
    if(!req.body.password){
        res.status(400).send('password required');
        return;
    }

    User.findOne({$or:[{'username': req.body.username}, {'email': req.body.email}]}, function(err, user){
        if (err) {
            res.status(500).send(err);
            return
        }

        if (!user) {
            res.status(401).send('Invalid Credentials');
            return;
        }
        user.comparePassword(req.body.password, function(err, isMatch) {
            if(!isMatch || err){
                res.status(401).send('Invalid Credentials');
            } else {
                res.status(200).json({token: createToken(user), currentUserId: user._id});
            }
        });
    });

};

module.exports.signup = function(req, res){
    if(!req.body.username){
        res.status(400).send('username required');
        return;
    }
    if(!req.body.email){
        res.status(400).send('email required');
        return;
    }
    if(!req.body.password){
        res.status(400).send('password required');
        return;
    }
    if(!req.body.firstName){
        res.status(400).send('firstName required');
        return;
    }
    if(!req.body.lastName){
        res.status(400).send('lastName required');
        return;
    }

    User.findOne({$or:[{'username': req.body.username}, {'email': req.body.email}]}, function(err, user){
        if (err) {
            res.status(500).send(err);
            return
        }
        if (user) {
            res.status(400).send('Username or email already exists');
            return;
        }

        var user = new User();

        user.username = req.body.username;
        user.email = req.body.email;
        user.password = req.body.password;
        user.firstName = req.body.firstName;
        user.lastName = req.body.lastName;

        if(req.body.charge == 'PROFESSIONAL' || req.body.charge == 'STUDENT') {
            user.charge = req.body.charge;
            if(req.body.charge == 'STUDENT') {
                user.license = new Date().setFullYear(new Date().getFullYear() + 1);
            }
        }

        user.save(function(err) {
            if (err) {
                res.status(500).send(err);
                return;
            }

            res.status(201).json({token: createToken(user)});
        });
    });
};

module.exports.unregister = function(req, res) {
    req.user.remove().then(function (user) {
        res.sendStatus(200);
    }, function(err){
        res.status(500).send(err);
    });
};

function createToken(user) {
    var tokenPayload = {
        user: {
            _id: user._id,
            username: user.username
        }

    };
    return jwt.encode(tokenPayload,Config.auth.jwtSecret);
}

module.exports.pushCommentId = function(userId, commentId, res, m) {
    User.findByIdAndUpdate(userId, {"$push": {comments: commentId}},
        {new: true}, function (err) {
            if (err) {
                return res.status(500).send(err);
            }
            return res.status(200).json(m);
        });
};

module.exports.pushUserLinkId = function(userId, userLinkId, res, m) {
    User.findByIdAndUpdate(userId, {"$push": {links: userLinkId}},
        {new: true}, function (err) {
            if (err) {
                return res.status(500).send(err);
            }
            return res.status(200).json(m);
        });
};

module.exports.deleteCommentId = function(userId, commentId, res, m) {
    User.findByIdAndUpdate(userId, {"$pull": {comments: commentId}},
        {new: true}, function (err) {
            if (err) {
                return res.status(500).send(err);
            }
            return res.status(200).json(m);
        });
};

module.exports.deleteUserLinkId = function(userId, userLinkId, res, m) {
    User.findByIdAndUpdate(userId, {"$pull": {links: userLinkId}},
        {new: true}, function (err) {
            if (err) {
                return res.status(500).send(err);
            }
            return res.status(200).json(m);
        });
};

module.exports.postBookmark = function(req, res) {
    if (!req.body.bookmark) {
        res.status(400).send("Bookmark id required");
    }
    req.params.paragraph_id = req.body.bookmark;
    paragraphHelperController.getParagraph(req, res, function(paragraph) {
        User.findByIdAndUpdate(req.user._id, {"$addToSet": {bookmarks: {paragraph: paragraph._id, title: paragraph.lawCode +
        " " + paragraph.title}}},
            {new: true}, function (err, data) {
                if (err) {
                    return res.status(500).send(err);
                }
                return res.status(200).json(data.bookmarks);
        });
    });
}

module.exports.deleteBookmark = function(req, res) {
    User.findByIdAndUpdate(req.user._id, {"$pull": {bookmarks: {paragraph: req.params.bookmark_id}}},
        {new: true}, function (err) {
            if (err) {
                return res.status(500).send(err);
            }
            return res.sendStatus(200);
        });
};

module.exports.publicProfile = function(req, res) {
    User.findById(req.params.user_id, {'_id': 0,'firstName':1, 'lastName':1, 'username':1, 'bookmarks':1, 'links':1,
        'comments':1, 'charge':1}, function (err, user) {
            if (err) {
                return res.status(500).send(err);
            }
            return res.json(user);
        });
};