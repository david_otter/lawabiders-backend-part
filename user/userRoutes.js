module.exports = userRoutes;

function userRoutes(passport) {

    var userController = require('./userController');
    var router = require('express').Router();


    router.post('/login', userController.login);
    router.post('/signup', userController.signup);
    router.post('/unregister', passport.authenticate('jwt', {session: false}),userController.unregister);
    router.post('/api/users/bookmarks', userController.postBookmark);
    router.delete('/api/users/bookmarks/:bookmark_id', userController.deleteBookmark);
    router.get('/api/users/:user_id', userController.publicProfile);
    
    return router;

}